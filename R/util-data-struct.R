#' Checks if all columns in a data frame are factors.
#' 
#' @param x a \code{data.frame}
#' @keywords internal
all_are_factors <- function(x) {
  stopifnot(is.data.frame(x))
  all(vapply(x, is.factor, FUN.VALUE = logical(1)))
}